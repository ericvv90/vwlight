// pendiente, meter a su propio modulo
fs = require('fs');

module.exports ={

	readState :  function() {
		return new Promise(function(resolve, reject){
			fs.readFile('./data/state.js', 'utf8', function (err,data) {
			  if (err) {
			   	return reject(err)
			  }
			  var data = JSON.parse(data)
			  return resolve(data)
			});	
		})
	},
	
	saveState : function(state){
		return new Promise(function(resolve, reject){
	 	// convert data to string
		  stateString = JSON.stringify(state);
		  fs.writeFile("data/state.js", stateString, function(err) {
		      if(err) {
		        return reject(err)
		      }
		      return resolve(true)
		     
		  }); 
		})
	},

	resetState : function(){

	  var state={
	        screen1:{
	          video_id:"",
	          user_id:""
	        },
	        screen2:{
	          video_id:"",
	          user_id:""
	        },
	        screen3:{
	          video_id:"",
	          user_id:""
	        },
	        screen4:{
	          video_id:"",
	          user_id:""
	        },  
	        fullScreen:{
	          video_id:"",
	          user_id:""
	        }
	      }
	    return module.exports.saveState(state).then(function(){
	        return true
	      }).catch(function(err){
	        console.log(err)
	        return err
	      })  
  //
}

}

