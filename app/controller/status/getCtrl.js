var fileState = require.main.require("./app/lib/fileState.js")

module.exports = { 

	// Obtener todos los cofepris
	getStatus : function (req, res) {
			fileState.readState().then(function(data){
				res.json(data)
			}).catch(function(err){
				return res.status(409).json(err)   
			})
	},

	getList : function(req,res){
		//mover a una promesa
		fs.readFile('./data/videos.json', 'utf8', function (err,data) {
		  if (err) {
		   	return res.json({err:err})
		  }
		  try{
		  	var data = JSON.parse(data)
		  }catch(err){
		  	console.log(err)
				return res.json({err:err})
		  }
		  
		  return res.json(data)
		});	
		
	},

	getInfo : function(req,res){
		//mover a una promesa
		fs.readFile('./data/infovideos.json', 'utf8', function (err,data) {
		  if (err) {
		   	return res.json({err:err})
		  }
		  try{
		  	var data = JSON.parse(data)
		  }catch(err){
		  	console.log(err)
				return res.json({err:err})
		  }
		  return res.json(data)
		});	
		
	},

	getImages : function(req,res){
		//mover a una promesa
		var files = fs.readdirSync('./public/images/carousel');
		  return res.json(files)
		
	}

}

