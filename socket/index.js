// index.js
var fs = require('fs');
var fileState = require.main.require("./app/lib/fileState.js")


// on start reset state
fileState.resetState().catch(function(err){
  console.log(err)
  return err
});

module.exports = function(io){
	io.on('connection', function(socket){
	  console.log('a user connected');
     socket.on('disconnect', function(){
        console.log('user disconnected');
      });

		  socket.on('playVideo', function(data){
        //pendiente: meter en su propio modulo
        fileState.readState().then(function(state){
            state[data.screen_selected].video_id = data.video_id; 
            state[data.screen_selected].user_id = data.user_id; 
            console.log("state to save")
            console.log(state)
            return fileState.saveState(state)})
          .then(function(){
            io.emit('playVideo', data);
          }).catch(function(err){
            console.log(err);
            return err   
          })
		  });

      socket.on('resetState', function(){
        console.log("executing resetState");
        fileState.resetState().then(function(){
          io.emit('resetState', "no data needed");
        }).catch(function(err){
          console.log(err)
          return err
        });
      });


      socket.on('quitVideo', function(data){
        console.log("executing quitVideo");
        console.log(data)
        fileState.readState().then(function(state){
            state[data.screen_selected].video_id = ""; 
            state[data.screen_selected].user_id = ""; 
            console.log("state to save")
            console.log(state)
            return fileState.saveState(state)})
          .then(function(){
            io.emit('quitVideo', data);
          }).catch(function(err){
            console.log(err);
            return err   
          })
      });

      socket.on('videoEnded', function(data){
        console.log("executing videoEnded");
        console.log(data)
        fileState.readState().then(function(state){
            state[data.screen_id].video_id = ""; 
            state[data.screen_id].user_id = ""; 
            console.log("state to save")
            console.log(state)
            return fileState.saveState(state)})
          .then(function(){
            //data.screen_selected={}
            
            data.screen_selected=data.screen_id
            //data.screen_selected.video_id=data.video_id
            io.emit('videoEnded', data);
          }).catch(function(err){
            console.log(err);
            return err   
          })
      });


	});
	
  // return module.exports = function(io)
	return true;
}

