 // var audioSource="/audio/"
var audioSource= "http://132.248.177.14:3005/audio/"
var totalMedia=0
var audiosLoaded=0
    var currentState={
        screen1:{
          video_id:"",
          user_id:""
        },
        screen2:{
          video_id:"",
          user_id:""
        },
        screen3:{
          video_id:"",
          user_id:""
        },
        screen4:{
          video_id:"",
          user_id:""
        },  
        fullScreen:{
          video_id:"",
          user_id:""
        }
      }

      var videoInfoDisplayed="";

      var videoDescription={}

  var videoPlaying={
    screen_selected:"",
    user_id:"",
    video_id:""
  }

$(document).ready(function(){
    //populate list videos
    $("#videoIframe").hide()
    getList().then(function(data){
       populateList(data)
       loadAudios(data)
       //setVideosId(data)
    }).catch(function(err){
      console.log(err)
    })

    getInfoVideos().then(function(data){
       videoDescription = data
      console.log(videoDescription)
    }).catch(function(err){
      console.log(err)
    })

    if(localStorage.getItem("user_id")===null){
        var d=new Date()
        d=d.getTime()
        localStorage.setItem('user_id', d);
    }

    // start socket io connection
    var socket = io();

    // socket.on('updateStatus', function(msg){
    //   alert("updating status")
    // });
    socket.on('playVideo', function(data){
      updateState(data);
    });
    socket.on('resetState', function(){
      console.log("resetState")
      resetState();
    });

    socket.on('quitVideo', function(data){

      updateStateFromQuitVideo(data)
      // stop audio if it was me who quit the video

      console.log("executing socket on quitVideo")
    });

    socket.on('videoEnded', function(data){

      console.log("executing socket on videoEnded")
      console.log(data)
      updateStateFromEndedVideo(data)
      // stop audio if it was me who quit the video
    });

    $('.collapsible').collapsible();
  // se tiene que inicializar el modal ... punto menos para materialize
    $('.modal').modal();

    initializePlayButtons();

    $("#section_cemie_sol").click(function(){
      var dirtyId=this.id;
      videoInfoDisplayed = dirtyId.replace("section_","")
      console.log(videoInfoDisplayed)
      setInfoInNav(videoInfoDisplayed);
      openNav()
    })

    $("#section_apprendoestructofa").click(function(){
      var dirtyId=this.id;
      videoInfoDisplayed = dirtyId.replace("section_","")
      console.log(videoInfoDisplayed)
      setInfoInNav(videoInfoDisplayed);
      openNav()
    })
    
    

  	$("#screen1Option").click((event)=>{
      console.log("sending message on socket")
  		var screen_selected= "screen1"
      videoSelected=videoInfoDisplayed
  		selectVideo(videoSelected, screen_selected, socket)
      showVideoIsPlaying(videoSelected, screen_selected)
  	})
 	  $("#screen2Option").click((event)=>{
 		var screen_selected= "screen2"
     videoSelected=videoInfoDisplayed
  		selectVideo(videoSelected, screen_selected, socket)
      showVideoIsPlaying(videoSelected, screen_selected)
  	})
  	$("#screen3Option").click((event)=>{
  		var screen_selected= "screen3"
       videoSelected=videoInfoDisplayed
  		selectVideo(videoSelected, screen_selected, socket)
      showVideoIsPlaying(videoSelected, screen_selected)
  	})
  	$("#screen4Option").click((event)=>{
  		var screen_selected= "screen4"
       videoSelected=videoInfoDisplayed
  		selectVideo(videoSelected, screen_selected, socket)
      showVideoIsPlaying(videoSelected, screen_selected)
  	});

    $("#optionFullScreen").click(()=>{
      var screen_selected= "fullScreen"
       videoSelected=videoInfoDisplayed
      selectVideo(videoSelected, screen_selected, socket)
      showVideoIsPlaying(videoSelected, screen_selected)
    })

    $("#quitVideo").click(()=>{
      // pendiente:loading
      closeNav()
      socket.emit("quitVideo", videoPlaying);
    })

    getState().then(function(data){
      // when app starts, set screens 
      currentState=data;
      console.log("state was set");
      console.log(currentState);
      onGetState(data);
      //askForState(currentState);
    }).catch(function(err){
      console.log(err)
    });

    $("#playVideowall").click(function(){
        $("#navOptionsWrapper").hide(200)
        $("#videoWallOptions").show(200)

    })
    
    $("#returnToList").click(function(){
      console.log("hiding videoWallOptions");
        $("#videoImageWrapper").show()
        $("#navOptionsWrapper").show(200)
        $("#videoWallOptions").hide(200)
        $("#videoIframe").hide()
        $("#videoIframe").attr("src","")

    })

    $("#playHere").click(function(){
      $("#videoImageWrapper").hide()
      $("#navOptionsWrapper").hide()
      $("#videoIframe").show()
      // set video    videoInfoDisplayed
      console.log("videoInfoDisplayed: "+videoInfoDisplayed)
      //console.log("videoInfoDisplayed")
      //console.log(videoDescription[videoInfoDisplayed].youtube)
      var youtube_src="https://www.youtube.com/embed/"+videoDescription[videoInfoDisplayed].youtube+"?autoplay=1"
       $("#videoIframe").attr("src",youtube_src)
    })

})


function selectVideo(video_id, screen_selected, socket){
    //playAudio(video_id);

    var audio_id="audio_"+video_id
    console.log("audio to play:");
    console.log(audio_id);
    document.getElementById(audio_id).play();
    document.getElementById(audio_id).pause();
  	var data={
  		video_id:video_id,
  		screen_selected:screen_selected,
      user_id: localStorage.getItem("user_id")
  	};
  socket.emit("playVideo", data);

}

function getState(){
  return new Promise (function(resolve,reject){
      $.ajax({ 
         url: "/screen/state",
         method: "get",
           success: function (data) {
              //console.log("data coming from server");
              //console.log(data);
              resolve(data);

          },
          error: function (ajaxContext) {
              reject(ajaxContext.responseText);
          }
      })
  })
}


function checkFullScreenAvailable(state){
  if(areScreensEmpty(state)){
    $("#optionFullScreen").show();
  }else{
     $("#optionFullScreen").hide();
  }
}

function areScreensEmpty(newState){
    var jQuery_each_result=true;
    jQuery.each(newState, function(screen, video) {
      if(video.video_id!=""){
        jQuery_each_result=false;
        return false;
      }
  });
    if(jQuery_each_result){
      return true;
    }else{
      return false;
    }
}

function playAudio(video_id){
      //$("#audio1").play();
    var audio_id="audio_"+video_id
      //document.getElementById(audio_id)currentTime=0;
      document.getElementById(audio_id).play(); 
      // setTimeout(function(){
      // document.getElementById(audio_id).play();        
      // },500)
}

function initializePlayButtons(){

    $("#selectVideo1").click(()=>{
      videoSelected="video1";
      $("#selectScreenModal").modal('open');
    })

    $("#selectVideo2").click(()=>{
      videoSelected="video2";
      $("#selectScreenModal").modal('open');
    })

    $("#selectVideo3").click(()=>{
      videoSelected="video3";
      $("#selectScreenModal").modal('open');
    })

    $("#selectVideo4").click(()=>{
      videoSelected="video4";
      $("#selectScreenModal").modal('open');
    })
};

function updateState(data){

  // update state
  currentState[data.screen_selected].user_id = data.user_id;
  currentState[data.screen_selected].video_id = data.video_id;

  console.log("current state in updateState");
  console.log(currentState)

  // play audio if it was me
  if(data.user_id===localStorage.getItem("user_id")){
    if(data.screen_selected!=="fullScreen"){
           playAudio(data.video_id)
      $("#turnUpVolume").show()    
    }else{

    $("#turnUpVolume").hide()
    }
    $("#screenPlayingVideo").html(data.screen_selected)
      videoPlaying.user_id=data.user_id
      videoPlaying.screen_selected=data.screen_selected
      videoPlaying.video_id=data.video_id
  }

  // hide, if necesary, full screen video
  checkFullScreenAvailable(currentState);
  updatePlayButtons(currentState)
}

function updateStateFromQuitVideo(data){
  // update state
  // play audio if it was me
  console.log("executing updateStateFromQuitVideo")
  //alert(localStorage.getItem("user_id")+" "+data.user_id);
  if(data.user_id===localStorage.getItem("user_id")){
    if(data.screen_selected!=="fullScreen"){
     stopAudio(data.video_id)
     videoPlaying.user_id=data.user_id
     videoPlaying.screen_selected=data.screen_selected
     videoPlaying.video_id=data.video_id
    }
  }
  currentState[data.screen_selected].user_id = "";
  currentState[data.screen_selected].video_id = "";
  // hide, if necesary, full screen video
  checkFullScreenAvailable(currentState);
  updatePlayButtons(currentState)
}

function updateStateFromEndedVideo(data){
  // update state
  // play audio if it was me
  console.log("executing updateStateFromEndedVideo")
  //alert(localStorage.getItem("user_id")+" "+data.user_id);
  if(data.user_id===localStorage.getItem("user_id")){
    if(data.screen_selected!=="fullScreen"){
     stopAudio(data.video_id)
     videoPlaying.user_id=data.user_id
     videoPlaying.screen_selected=data.screen_selected
     videoPlaying.video_id=data.video_id
    }
    closeNav()
  }
  currentState[data.screen_selected].user_id = "";
  currentState[data.screen_selected].video_id = "";
  // hide, if necesary, full screen video
  checkFullScreenAvailable(currentState);
  updatePlayButtons(currentState)
}

function updatePlayButtons(state){

  if(state.fullScreen.video_id!=""){
    $(".playBtn").hide()
    $("#playVideowall").hide()
    return
  }
  $("#playVideowall").show()
  if(areScreensEmpty(state)){
    console.log("las pantallas están vacias")
    $(".playBtn").show()
    return
  }

  jQuery.each(state, function(screen, video) {
      if(video.video_id!=""){
        //var playBtnId= replace('stringToReplace','');
        console.log(video.video_id);
        $("#"+screen+"Option").hide();
      }else{
        $("#"+screen+"Option").show();
      }
  });
}

function onGetState(currentState){
    // hide, if necesary, full screen video
    checkFullScreenAvailable(currentState);
    updatePlayButtons(currentState)
}

function resetState(){
  currentState={
          screen1:{
            video_id:"",
            user_id:""
          },
          screen2:{
            video_id:"",
            user_id:""
          },
          screen3:{
            video_id:"",
            user_id:""
          },
          screen4:{
            video_id:"",
            user_id:""
          },  
          fullScreen:{
            video_id:"",
            user_id:""
          }
  }

  videoPlaying={
    screen:"",
    user_id:"",
    video_id:""
  }
    checkFullScreenAvailable(currentState);
    updatePlayButtons(currentState)
    //if any
    stopAllAudios()
    closeNav()

}

function stopAllAudios(){
      $('.audio').each(function(){
          this.pause(); // Stop playing
          this.currentTime = 0; // Reset time
      }); 
}


function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    $("#navOptionsWrapper").show(200)
     $("#returnToList").show(200)
    $("#videoIsPlaying").hide(200)
    $("#videoWallOptions").hide(200)
    
    
    document.getElementById("myNav").style.width = "0%";
}

function stopAudio(video_id){
       stopAllAudios()
      // var audio_id=video_id+"_audio"
      // console.log(audio_id)
      // document.getElementById(audio_id).stop(); 
}

function setInfoInNav(id){
  console.log(id);
  // set video image
  var sourceImage="images/"+id+".png"
  $("#videoImage").attr("src",sourceImage);
  console.log(videoDescription)

  $("#videoName").html(videoDescription[id].name)
  $("#videoDuration").html(videoDescription[id].duration)
  $("#videoDescription").html(videoDescription[id].description)
}

function showVideoIsPlaying(videoSelected, screen_selected){
  $("#videoIsPlaying").show(200);
  $("#videoWallOptions").hide(200);
  $("#returnToList").hide(200);
  
}

function getList(){
      return new Promise (function(resolve,reject){
      $.ajax({ 
         url: "/list/",
         method: "get",
           success: function (data) {
              //console.log("data coming from server");
              //console.log(data);
              resolve(data);

          },
          error: function (ajaxContext) {
              reject(ajaxContext.responseText);
          }
      })
  })
}

function getInfoVideos(){
      return new Promise (function(resolve,reject){
      $.ajax({ 
         url: "/list/info",
         method: "get",
           success: function (data) {
              //console.log("data coming from server");
              //console.log(data);
              resolve(data);

          },
          error: function (ajaxContext) {
              reject(ajaxContext.responseText);
          }
      })
  })
}

function populateList(data){
  for (var i = 0; i < data.array_videos.length; i++) {
  var html = '<div class="divider"></div>'+
              '<div class="section" >'+
              '<div class="row" id="section_'+data.array_videos[i].id+'" onclick="showInfoVideo(this.id)" >'+
              '<div class="col s4" >'+
              '<img src="images/'+data.array_videos[i].id+'.png"   width="100%" height="100%">'+
              '</div>'+
              '<div class="col s8">'+
              '<h3 style="font-weight: bold">'+data.array_videos[i].name+'</h3>'+
              '</div>'+
              '</div>'+
              '</div>'
  $("#videoListWrapper").append(html);
  }
}

function setVideosId(data){
  for (var i = 0; i < data.array_videos.length; i++) {
    $("#"+data.array_videos[i].id).click(function(){
      var dirtyId=this.id;
      videoInfoDisplayed = dirtyId.replace("section_","")
      console.log(videoInfoDisplayed)
      setInfoInNav(videoInfoDisplayed);
      openNav()
    })
  }
  console.log("executing setVideosId")
}


function showInfoVideo(id){
      var dirtyId=id;
      videoInfoDisplayed = dirtyId.replace("section_","")
      console.log(videoInfoDisplayed)
      setInfoInNav(videoInfoDisplayed);
      openNav()
}

function loadAudios(data){

  //totalMedia=data.array_videos.length
  totalMedia=1;
  for (var i = 0; i < data.array_videos.length; i++) {  //onloadeddata onloadeddata="myOnLoadedData(this.id)"
  var html =  '<audio class="audio"   controls id="audio_'+data.array_videos[i].id+'" style="visibility:hidden">'+
  '<source src="'+audioSource+data.array_videos[i].id+'.mp3" type="audio/mpeg">'+
  '</audio>'

  $("#audioSection").append(html);
  }
  myOnLoadedData()
}

function myOnLoadedData(){
  //audiosLoaded++
  //alert("executing")
  //if(totalMedia===audiosLoaded){
    //alert("se cargaron los videos")
    setTimeout(function(){
          $('#load-screen').delay(1000).fadeOut(function(){$(this).remove()}) 
    }, 0)

  //}

}
