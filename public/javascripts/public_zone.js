//var source = "http://132.248.177.14:3005/videos/HD/"
var source = "http://localhost:3005/videos/HD/"
//var sourceImages="http://132.248.177.14:3005/"
var sourceImages="http://localhost:3005"
var socket = io();
var showInstitutionalVideo1EachThisTime=480000
console.log("executing public_zone.js")
var allVideos=[]
var setIntervalsJson={
	screen1:function(){},
	screen2:function(){},
	screen3:function(){},
	screen4:function(){}
}

		var currentState={
	        screen1:{
	          video_id:"",
	          user_id:""
	        },
	        screen2:{
	          video_id:"",
	          user_id:""
	        },
	        screen3:{
	          video_id:"",
	          user_id:""
	        },
	        screen4:{
	          video_id:"",
	          user_id:""
	        },  
	        fullScreen:{
	          video_id:"",
	          user_id:""
	        }
	}

$(document).ready(function(){
	$('.modal').modal();
  // $('.carousel.carousel-slider').carousel({
  //   fullWidth: true
  // });
	
	$("#fourScreensContainer").hide();
	setTimeout(function(){
	$("#fullScreen").hide()
	$("#fourScreensContainer").show();
	populateCarousels();
	},5000)

    if(localStorage.getItem("user_id")===null){
        var d=new Date()
        d=d.getTime()
        localStorage.setItem('user_id', d);
    }

    getList().then(function(data){
    	//console.log("data from get list")
    	startProgram(data)
    	//console.log(data)
       // populateList(data)
       // loadAudios(data)
       //setVideosId(data)
    }).catch(function(err){
      console.log(err)
    })

	// start program



	$("#btnPlay").click(function(){
		document.getElementById("video1").play();
		document.getElementById("video2").play();
		document.getElementById("video3").play();
		document.getElementById("video4").play();
	});

	$("#btnStop").click(function(){
		document.getElementById("video1").pause();
		document.getElementById("video2").pause();
		document.getElementById("video3").pause();
		document.getElementById("video4").pause();
	})

	$("#fullScreen").click(function(){
		$(document).toggleFullScreen();
	})

	getState().then(function(data){
		// when app starts, set screens 
		updateScreens(currentState, data);
		currentState=data;
		console.log("state was set");
		console.log(currentState);
		//askForState(currentState);
		}).catch(function(err){
			console.log(err)
		});


	socket.on('playVideo', function(data){
		console.log("the state has changed");
		console.log(data);
		//updateScreens(currentState, data);
		currentState[data.screen_selected].video_id=data.user_id
		currentState[data.screen_selected].user_id=data.user_id
		updateScreenSocket(data);
	});
	socket.on('resetState', function(){
		currentState={
	        screen1:{
	          video_id:"",
	          user_id:""
	        },
	        screen2:{
	          video_id:"",
	          user_id:""
	        },
	        screen3:{
	          video_id:"",
	          user_id:""
	        },
	        screen4:{
	          video_id:"",
	          user_id:""
	        },  
	        fullScreen:{
	          video_id:"",
	          user_id:""
	        }
		}
		cleanScreens();
	});

	socket.on('quitVideo', function(data){
		console.log("on quit video");
		console.log(data);
		currentState[data.screen_selected].video_id=""
		currentState[data.screen_selected].user_id=""
		quitVideo(data);
	});
	
	socket.on('videoEnded', function(data){
		currentState[data.screen_selected].video_id=""
		currentState[data.screen_selected].user_id=""
		quitVideo(data);
	});


	$(document).keypress(function(e) {
		console.log(e.which);
		if(e.which == 102) {
			$(document).toggleFullScreen();
		}else if(e.which == 49){
			changeToFullScreen();
		}else if(e.which == 52){
			$("#fourScreensContainer").show();
			changeTo4Screens();
		}else if(e.which ===111){
			$("#optionsModal").modal('open');
		}else if(e.which ===114){
			clearAll(socket);
		}
	});

	$("#optionFullScreen").click(function(){
		changeToFullScreen();
	})

	$("#option4Screen").click(function(){
		changeTo4Screens();
	})

	$("#optionClearAll").click(function(){
		clearAll(socket);
	});
	$("#optionStopAll").click(function(){

		if(document.getElementById("video1") != null){
			document.getElementById("video1").pause();	
		}

		if(document.getElementById("video2") != null){
			document.getElementById("video2").pause();	
		}

		if(document.getElementById("video3") != null){
			document.getElementById("video3").pause();	
		}

		if(document.getElementById("video4") != null){
			document.getElementById("video4").pause();	
		}

	});

	$("#optionResumeAll").click(function(){
		if(document.getElementById("video1") != null){
			document.getElementById("video1").play();	
		}

		if(document.getElementById("video2") != null){
			document.getElementById("video2").play();	
		}

		if(document.getElementById("video3") != null){
			document.getElementById("video3").play();	
		}

		if(document.getElementById("video4") != null){
			document.getElementById("video4").play();	
		}
	});

	$("#optionFullScreenMode").click(function(){
		$(document).toggleFullScreen();
	})
	//setVideoOnScreen("video1", "div1")
})// end ready document


function askForState(currentState){
	getState().then(function(newState){
		// check if state has change
		if(hasChagedState(currentState, newState)){ // has changed
        	console.log("the state has changed");
        	//update state
        	updateScreens(currentState, newState);
        	currentState = newState;
        	// do changes in screens;

		}else{ // nothing has changed
         	//console.log("the state has not changed");
         	//update state
         	currentState = newState;
		};
			
		// ask again for the state
			setTimeout(function(){
				//console.log("asking for state")
				askForState(currentState);
			}, 500);
		}).catch(function(err){
			console.log(err)
		});


}

function getState(){
	return new Promise (function(resolve,reject){
			$.ajax({ 
	 		   url: "/screen/state",
	 		   method: "get",
	    	   success: function (data) {
	    	   		//console.log("data coming from server");
	    	   		//console.log(data);
	        		resolve(data);

	    		},
	    		error: function (ajaxContext) {
	        		reject(ajaxContext.responseText);
	   			}
			})
	})
}


function hasChagedState(currentState, newState){
	//console.log("currentState in hasChanged");
	//console.log(currentState);
		var currentStateString = JSON.stringify(currentState);
		var newStateString =  JSON.stringify(newState);
			//console.log("comparing objects");
			//console.log(currentStateString);
			//console.log("currentState in hasChanged");
			//console.log(currentState);
		if(currentStateString === newStateString){
				return false;
		}else{
				return true;
		}

}

function updateScreens(currentState, newState){
//   	console.log("-----------> current state");
//   	//console.log(currentState);
//   	jQuery.each(currentState, function(i, val) {
	//  	console.log("i: "+i);
	//  	console.log("val: "+val.video_id);
	// });
	// console.log("-----------> newState");
	// cambiar esto: actualizar solo el que sea necesario
	jQuery.each(newState, function(screen, video) {
		if(currentState[screen].video_id != video.video_id){
			if(video.video_id===""){

				if(screen==="fullScreen"){
					$("#"+screen).empty();
					  var html=    "<br>"+
					      "<br>"+
					      "<br>"+
					      "<br>"+
					      "<br>"+
					      '<h1 style="color:white"> FULL SCREEN VIDEO</h1>'
					   $("#"+screen).append(html);
				}else{

					$("#"+screen).empty();
					var html="<br>" 
	    			+"<br>"
	    			+"<h1>"+screen+"</h1>";
	    			$("#"+screen).append(html);		
				}


			}else{
				setVideoOnScreen(video.video_id, screen, video.user_id);
				if(screen==="fullScreen"){
					changeToFullScreen();
				}else{
					changeTo4Screens();
				}
			}

		}
	});

	// jQuery.each(newState, function(i, val) {
	// 	console.log("i: "+i);
	//  	console.log("val: "+val.video_id);
	// 	if(currentState[i].video_id != val.video_id){
	// 		setVideoOnScreen(val.video_id, i);
	// 		if(){

	// 		}
	// 	}
	// });

	// pendiente verificar que no exista un video corriendo en ese screen
}

function setVideoOnScreen(video_id, screen_id, user_id){
	var muted="muted"
	if(screen_id==="fullScreen"){
		muted="";
	}
	var video_text=		'<video controls autoplay id="'+video_id+'""  width="100%" height="100%" controls '+muted+'>'+
	  '<source src="'+source+video_id+'.mp4" type="video/mp4">'+
	  'Your browser does not support the video tag.'+
	   '</video>' 
	$("#"+screen_id).empty();
	$("#"+screen_id).append(video_text);    

	$('#'+video_id).on('ended',function(){
    	videoEnded(video_id, screen_id, user_id);
    });		
}

function hasChangedVideo(){

}

function changeToFullScreen(){
	$("#fourScreensContainer").hide();
	$("#fullScreen").show();
}

function changeTo4Screens(){
	$("#fourScreensContainer").show();
	$("#fullScreen").hide();
}

function clearAll(socket){
  socket.emit("resetState", "no data needed");

	// $.ajax({
	//   type: "POST",
	//   url: "/clearAll",
	// }).done((res)=>{
	// 	console.log(res);
	// })

}

function updateScreenSocket(data){
	console.log(data.screen_selected)
	setVideoOnScreen(data.video_id, data.screen_selected, data.user_id)
	if(data.screen_selected==="fullScreen"){
		changeToFullScreen();
	}else{
		changeTo4Screens();
	}
}

function cleanScreens(){
		var currentState={
	        screen1:{
	          video_id:"",
	          user_id:""
	        },
	        screen2:{
	          video_id:"",
	          user_id:""
	        },
	        screen3:{
	          video_id:"",
	          user_id:""
	        },
	        screen4:{
	          video_id:"",
	          user_id:""
	        },  
	        fullScreen:{
	          video_id:"",
	          user_id:""
	        }
	}

	jQuery.each(currentState, function(screen, video) {
			$("#"+screen).empty();
			// var html="<br>" 
			// +"<br>"
			// +"<h1>"+screen+"</h1>";
			var html='<img id="'+screen+'_image1" style="height: 100%;width: 100%" src="/images/escudo_unam_azul.jpg">'+
   			'<img id="'+screen+'_image2" style="height: 100%;width: 100%; display:none" src="/images/escudo_unam_azul.jpg">'

			$("#"+screen).append(html);		
			changeImage(allVideos, screen)
		})
}

function quitVideo(data){

	if(data.screen_selected==="fullScreen"){
		$("#"+data.screen_selected).empty();
		  var html=    "<br>"+
		      "<br>"+
		      "<br>"+
		      "<br>"+
		      "<br>"+
		      '<h1 style="color:white"> FULL SCREEN VIDEO</h1>'
		   $("#"+data.screen_selected).append(html);
		   changeTo4Screens();
	}else{
		//var car = data.screen_selected
		$("#"+data.screen_selected).empty();
		//var carNumber =data.screen_selected.replace("screen","");

		//console.log("carNumber: "+carNumber)
		console.log("executing quit video")
		console.log("all videos:")
		console.log(allVideos)
		console.log("data scree selected")
		console.log(data.screen_selected)
		//changeImage(allVideos, data.screen_selected)
		var html='<img id="'+data.screen_selected+'_image1" style="height: 100%;width: 100%" src="/images/escudo_unam_azul.jpg">'+
   			 '<img id="'+data.screen_selected+'_image2" style="height: 100%;width: 100%; display:none" src="/images/escudo_unam_azul.jpg">'

   		$("#"+data.screen_selected).append(html)
		changeImage(allVideos, data.screen_selected)

	}
		
}

// pass the carousel to autoplay (car1, car2, car3,)
function autoplay(car, fromQuit) {
	//console.log(car)
    $("#"+car).carousel('next');
    setTimeout(function(){
    	if(!fromQuit){
    		autoplay(car);
    	}
    	
    }, 5000);
}

function populateCarousels(){
	getListImages()
	.then(function(listImages){
		//console.log("list of images")
		//console.log(" executing populateCarousels")
		var numScreen=4;
		for (var i = 0; i < numScreen; i++) {
			console.log("inside fot loop")
			var ii=i+1;
			changeImage(listImages, "screen"+ii);
		}
		// console.log("loop finished")
		// for (var i = 1; i <= numScreen; i++) {
		// 	var shuffleArray = shuffle(listImages);
		// 	console.log("making car "+i);
		// 	var html='<div id="car'+i+'" class="carousel carousel-slider" style="height: 100%;width: 100%" >'
		// 	for (var ii = 0; ii < listImages.length; ii++) {	
		// 		html+= '<a class="carousel-item"><img style="height: 100%;width: 100%" src="'+sourceImages+'/images/carousel/'+shuffleArray[ii]+'"></a>'
		// 	}
		// 	html+='</div>'
		// 	$("#screen"+i).append(html);
		// 	$("#car"+i).carousel();
		// 	autoplay("car"+i , false)
		// }
	}).catch(function(err){
		console.log(err)
	})
}

function changeImage(listImages, screen) {
	//console.log(car)
	//sconsole.log("listImages")
	//console.log("executing changeImage")
	//console.log("----------> changeImage: "+screen);
	clearInterval(setIntervalsJson[screen])
	var totalImages=listImages.length
	var x = Math.floor((Math.random() * totalImages));
	//console.log("random number: "+x);
	//console.log("totalImages "+totalImages);
	var path= sourceImages+"/images/carousel/"+listImages[x];
	//console.log('$("#screen1_image2").css("display")')
	//console.log($("#screen1_image2").css("display"))//inline-block

	 if($("#"+screen+"_image2").css("display")==="none"){
	 	//console.log("image 2 is hidden")
	 	$("#"+screen+"_image2").attr("src",path);
		$("#"+screen+"_image1").hide(1200);
		$("#"+screen+"_image2").show(1000)
	}else{
		//console.log("image 1 is hidden")
		$("#"+screen+"_image1").attr("src",path);
		$("#"+screen+"_image2").hide(1200);
		$("#"+screen+"_image1").show(1000)
	}
	//console.log($("#screen1_image1").css("display"))
	//$("#screen1_image1").attr("src",path);
	//$("#screen1_image1").hide(1000);
	//console.log($("#screen1_image1").css("display"))
	setIntervalsJson[screen] = setInterval(function () {
        changeImage(listImages, screen);
    }, 30000);

    // setInterval(function(){
    // 		changeImage(listImages, screen);
    // }, 5000);
	
}


function getListImages(){
	return new Promise (function(resolve,reject){
			$.ajax({ 
	 		   url: "/list/images",
	 		   method: "get",
	    	   success: function (data) {
	    	   		//console.log("data coming from server");
	    	   		//console.log(data);
	    	   		allVideos=data
	    	   		//console.log("allVideos")
	    	   		//console.log(allVideos)
	        		resolve(data);

	    		},
	    		error: function (ajaxContext) {
	        		reject(ajaxContext.responseText);
	   			}
			})
	})
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

function videoEnded(video_id, screen_id, user_id){
	var data={}
	data.video_id=video_id
	data.screen_id=screen_id
	data.user_id=user_id
	console.log(data)
	socket.emit("videoEnded", data);
}

function startProgram(videos){
	// show images in full screen
	// setTimeout(function(){
	//  do nothing
	// },10000)

	// play institutional video
	setTimeout(function(){
		playVideoInFullScreen(videos)
		startProgram(videos)
	},showInstitutionalVideo1EachThisTime)
}

function playVideoInFullScreen(videos){

	var totalVideos = videos.array_videos.length
	var x = Math.floor((Math.random() * totalVideos));
    var jQuery_each_result=true;
    var videoToWatch = videos.array_videos[x].id
    //console.log("videoToWatch")
    //console.log(videoToWatch)
    //console.log("currentState")
    //console.log(currentState)
    jQuery.each(currentState, function(screen, video) {
      if(video.video_id!=""){
      	console.log("video.video_id: "+video.video_id)
        jQuery_each_result=false;
        return false;
      }
  	});
    if(jQuery_each_result){
	  	var data={
	  		video_id:videoToWatch,
	  		screen_selected:"fullScreen",
	    	user_id: localStorage.getItem("user_id")
	  	};
	  	socket.emit("playVideo", data);
    }else{
    console.log("fullScreen no disponible")
      return false;
    }
}

function getList(){
      return new Promise (function(resolve,reject){
      $.ajax({ 
         url: "/list/",
         method: "get",
           success: function (data) {
              //console.log("data coming from server");
              //console.log(data);
              resolve(data);

          },
          error: function (ajaxContext) {
              reject(ajaxContext.responseText);
          }
      })
  })
}







