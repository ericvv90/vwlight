
module.exports = function (app, io) {    
    app.use('/', require.main.require('./routes/all/zones')(io));
    app.use('/screen', require.main.require('./routes/all/screen')(io));
    app.use('/list', require.main.require('./routes/all/list')(io));
};