var statusCtrl = require.main.require('./app/controller/status/getCtrl.js');

var express = require('express');   
var router = express.Router();
router.get   ("/status", statusCtrl.getStatus); 
router.get   ("/list", statusCtrl.getList); 
module.exports = router; 