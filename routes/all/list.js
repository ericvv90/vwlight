// screens
var express = require('express');   
var router = express.Router();
var statusCtrl = require.main.require("./app/controller/status/getCtrl.js")
/* GET home page. */
      // leer state de un archivo
module.exports = function(io){

    router.get('/', statusCtrl.getList);
    router.get('/info', statusCtrl.getInfo);
    router.get('/images', statusCtrl.getImages);

	return router
}

//function(req, res, next) { res.json(state);


